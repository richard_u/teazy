export const NEW_DATETIME = 'NEW_DATETIME'
export const NEW_PAYOUT = 'NEW_PAYOUT'
export const RESET_DATETIMEPAYOUT = 'RESET_DATETIME'

export function newPayout(payout) {
  return { type: NEW_PAYOUT, payload: { payout } }
}

export function newDateTime(dateTime) {
  return { type: NEW_DATETIME, payload: { dateTime } }
}

export function resetDateTimePayout() {
  return { type: RESET_DATETIMEPAYOUT }
}
