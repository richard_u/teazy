export const REMOVE_ADDRESS_AND_LATLNG = 'REMOVE_ADDRESS_AND_LATLNG'
export const ADD_ADDRESS = 'ADD_ADDRESS'
export const ADD_LATLNG = 'ADD_LATLNG'

export function removeAddressAndLatLng() {
  return { type: REMOVE_ADDRESS_AND_LATLNG }
}

export function addAddress(address) {
  return { type: ADD_ADDRESS, payload: { address } }
}

export function addLatLng(latLng) {
  return { type: ADD_LATLNG, payload: { latLng } }
}
