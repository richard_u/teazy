export const NEXT_STEP = 'NEXT_STEP'
export const PREVIOUS_STEP = 'PREVIOUS_STEP'
export const RESET_STEPPER = 'RESET_STEPPER'
export const TOGGLE_DIALOG = 'TOGGLE_DIALOG'

export function toggleDialog() {
  return { type: TOGGLE_DIALOG }
}

export function nextStep() {
  return { type: NEXT_STEP }
}

export function previousStep() {
  return { type: PREVIOUS_STEP }
}

export function resetStepper() {
  return { type: RESET_STEPPER }
}
