export const ADD_QUOTE = 'ADD_QUOTE'
export const DELETE_INFO_SERVER_RESPONSE = 'DELETE_INFO_SERVER_RESPONSE'
export const GET_CONTRACT_STORAGE = 'GET_CONTRACT_STORAGE'
export const MAKE_NEW_QUOTES_STORE = 'MAKE_NEW_QUOTES_STORE'
export const MAKE_NEW_QUOTES_OBJECT = 'MAKE_NEW_QUOTES_OBJECT'
export const REHYDRATE_QUOTES_FROM_STORAGE = 'REHYDRATE_QUOTES_FROM_STORAGE'
export const REMOVE_FROM_QUOTES_STORAGE = 'REMOVE_FROM_QUOTES_STORAGE'
export const STORE_INFO_SERVER_RESPONSE = 'STORE_INFO_SERVER_RESPONSE'

export const addQuote = quote => {
  return { type: ADD_QUOTE, payload: { quote } }
}

export const deleteInfoResponse = () => {
  return { type: DELETE_INFO_SERVER_RESPONSE }
}

export const getContractStorage = storage => {
  return { type: GET_CONTRACT_STORAGE, payload: { storage } }
}

export const makeNewQuotesStore = quotes => {
  return { type: MAKE_NEW_QUOTES_STORE, payload: { quotes } }
}

export const makeNewQuotesObject = (queued, resolving, resolved) => {
  return {
    type: MAKE_NEW_QUOTES_OBJECT,
    payload: { queued, resolving, resolved }
  }
}

export const storeInfoResponse = info => {
  return {
    type: STORE_INFO_SERVER_RESPONSE,
    payload: info
  }
}

export const rehydrateQuotes = () => {
  const quotes = JSON.parse(localStorage.getItem('quotes'))
  return { type: REHYDRATE_QUOTES_FROM_STORAGE, payload: { quotes } }
}

export const removeQuote = nonce => {
  return { type: REMOVE_FROM_QUOTES_STORAGE, payload: { nonce } }
}
