import React, { Component } from 'react'
import PropTypes, { array, string } from 'prop-types'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import TableRow from './TableRow'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Dialog from '@material-ui/core/Dialog'
import AppBar from '@material-ui/core/AppBar'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'
import Slide from '@material-ui/core/Slide'
import moment from 'moment'

const styles = {
  label: {
    fontWeight: 600
  },
  appBar: {
    position: 'relative'
  },
  flex: {
    flex: 1
  },
  button: {
    float: 'right'
  },
  tabs: {
    display: 'inline'
  },
  section: {
    position: 'relative'
  }
}

function Transition(props) {
  return <Slide direction="up" {...props} />
}

class FullScreenDialog extends Component {
  state = { value: 0 }

  handleChange = (_event, value) => {
    this.setState({ value })
  }

  sortByDate = quotes => {
    return quotes.sort((q1, q2) => {
      return moment.unix(q2.t) - moment.unix(q1.t)
    })
  }

  sortByDateExpiration = quotes => {
    return quotes.sort((q1, q2) => {
      return moment.unix(q1.expiration) - moment.unix(q2.expiration)
    })
  }

  fillWithQuotes = (quotes, info) => {
    const sortedQuotes = this.sortByDate(quotes)
    if (sortedQuotes.length !== 0) {
      return sortedQuotes.map((quote, id) => {
        return (
          <div key={id}>
            <TableRow
              quote={quote}
              info={info}
              toggleWindow={this.props.toggleWindow}
            />
          </div>
        )
      })
    } else return null
  }

  fillAll = (quotes, info) => {
    const resolved = this.sortByDate(quotes.resolved).map((quote, id) => {
      return (
        <div key={id}>
          <TableRow
            quote={quote}
            info={info}
            toggleWindow={this.props.toggleWindow}
          />
        </div>
      )
    })
    const resolving = this.sortByDate(quotes.resolving).map((quote, id) => {
      return (
        <div key={id}>
          <TableRow
            quote={quote}
            info={info}
            toggleWindow={this.props.toggleWindow}
          />
        </div>
      )
    })
    const queued = this.sortByDateExpiration(quotes.queued).map((quote, id) => {
      return (
        <div key={id}>
          <TableRow
            quote={quote}
            info={info}
            toggleWindow={this.props.toggleWindow}
          />
        </div>
      )
    })
    return (
      <div>
        {queued}
        {resolving}
        {resolved}
      </div>
    )
  }

  render() {
    const { classes, quotes, info, open, toggleWindow } = this.props
    const { value } = this.state
    return (
      <Dialog
        fullScreen
        open={open}
        onClose={toggleWindow}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <section className={classes.section}>
            <div className={classes.button}>
              <IconButton
                color="inherit"
                onClick={this.props.toggleWindow}
                aria-label="Close"
              >
                <CloseIcon />
              </IconButton>
            </div>
            <div className={classes.tabs}>
              <Tabs value={value} onChange={this.handleChange}>
                <Tab className={classes.label} label="All" />
                <Tab className={classes.label} label="Paid" />
                <Tab className={classes.label} label="Resolved" />
              </Tabs>
            </div>
          </section>
        </AppBar>
        {value === 0 && this.fillAll(quotes, info)}
        {value === 1 && this.fillWithQuotes(quotes.resolving, info)}
        {value === 2 && this.fillWithQuotes(quotes.resolved, info)}
      </Dialog>
    )
  }
}

FullScreenDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  quotes: PropTypes.objectOf(array).isRequired,
  info: PropTypes.objectOf(string),
  toggleWindow: PropTypes.func.isRequired
}

const mapStateToProps = state => {
  return {
    quotes: state.app.quotes,
    info: state.app.info
  }
}

export default withStyles(styles)(connect(mapStateToProps)(FullScreenDialog))
