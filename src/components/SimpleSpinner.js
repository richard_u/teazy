import React from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'

const Spinner = ({ size, thickness }) => (
  <CircularProgress size={size} thickness={thickness} disableShrink />
)

export default Spinner
