import React from 'react'
import PropTypes from 'prop-types'
import { Circle, Map, TileLayer } from 'react-leaflet'

import '../assets/css/Map.css'

// const url = `https://maps.tilehosting.com/styles/bright/{z}/{x}/{y}{r}.png?key=${
//   process.env.REACT_APP_MAP_TILER_APIKEY
// }`

const url = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
// 'https://api.maptiler.com/styles/bright/{z}/{x}/{y}{r}.png?key=65l1qi9zWm4rAF60i8nq'

const LeafletMap = ({ handleClick, latLng, viewport, controlViewport }) => (
  <Map
    className="map"
    onViewportChanged={controlViewport}
    center={Object.values(latLng)}
    viewport={viewport}
    onClick={handleClick}
    zoomControl={false}
  >
    <TileLayer
      attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
      url={url}
    />
    <Circle center={latLng} radius={200} />
  </Map>
)

LeafletMap.propTypes = {
  handleClick: PropTypes.func,
  latLng: PropTypes.object
}

export default LeafletMap
