import React from 'react'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'
import SimpleSpinner from './SimpleSpinner'
import { mtezToTez } from '../utils'

const ConditionsTable = ({ address, dateTime, payout, quote }) => (
  <Table>
    <TableBody>
      <TableRow>
        <TableCell>Date</TableCell>
        <TableCell>{dateTime.format('D/M/YYYY H:mm')}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell>Address</TableCell>
        <TableCell>{address}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell>Payout</TableCell>
        <TableCell>{`${mtezToTez(payout)} ꜩ`}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell>Price</TableCell>
        <TableCell>
          {quote ? (
            `${mtezToTez(quote.price)} ꜩ`
          ) : (
            <SimpleSpinner size={18} thickness={4} />
          )}
        </TableCell>
      </TableRow>
    </TableBody>
  </Table>
)

export default ConditionsTable
