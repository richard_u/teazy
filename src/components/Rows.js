import React from 'react'
import { connect } from 'react-redux'
import moment from 'moment'
import { mapStatusToColour, mtezToTez } from '../utils'
import { addLatLng } from '../actions/map-actions'
import {
  Button,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
  DialogActions,
  withStyles
} from '@material-ui/core'

const mapActionsToProps = {
  addLatLng
}

const styles = {
  paper: {
    margin: 10
  }
}

const NotPaidRow = ({
  addLatLng,
  classes,
  open,
  toggleModal,
  quote,
  toggleWindow
}) => {
  const showLocation = () => {
    addLatLng(quote.latLng)
    toggleModal()
    toggleWindow()
  }

  return (
    <Dialog
      open={open}
      onClose={toggleModal}
      classes={{
        paper: classes.paper
      }}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{'Quote detail'}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Date:{' '}
          {moment.unix(quote.t).format('D/M/YYYY H:mm') +
            ' ( ' +
            moment.unix(quote.t).fromNow() +
            ' )'}
        </DialogContentText>
        <DialogContentText id="alert-dialog-description">
          Expiration:{' '}
          {moment.unix(quote.expiration).format('D/M/YYYY H:mm') +
            ' ( ' +
            moment.unix(quote.expiration).fromNow() +
            ' )'}
        </DialogContentText>
        <DialogContentText id="alert-dialog-description">
          Location:{' '}
          <button className="dialog-button-link" onClick={showLocation}>
            {quote.address}
          </button>
        </DialogContentText>
        <DialogContentText id="alert-dialog-description">
          Payout: {mtezToTez(quote.payout) + 'ꜩ'}
        </DialogContentText>
        <DialogContentText id="alert-dialog-description">
          Price: {mtezToTez(quote.price) + 'ꜩ'}
        </DialogContentText>
        <DialogContentText
          id="alert-dialog-description"
          style={{ color: mapStatusToColour(quote) }}
        >
          Status: {quote.status}
        </DialogContentText>
        <DialogContentText id="alert-dialog-description">
          <a
            className="dialog-scl"
            target="_blank"
            rel="noopener noreferrer"
            href={`https://tw.smartcontractlabs.ee/r/#${quote.encoded}`}
          >
            Pay with Smart Contract Labs Web Wallet
          </a>
        </DialogContentText>
      </DialogContent>

      <DialogActions>
        <Button
          onClick={toggleModal}
          color="primary"
          variant="contained"
          autoFocus
        >
          OK
        </Button>
      </DialogActions>
    </Dialog>
  )
}

const ResolvedResolvingRow = ({
  addLatLng,
  classes,
  open,
  toggleModal,
  info,
  quote,
  toggleWindow
}) => {
  const showLocation = () => {
    addLatLng(quote.latLng)
    toggleModal()
    toggleWindow()
  }
  return (
    <Dialog
      open={open}
      onClose={toggleModal}
      classes={{
        paper: classes.paper
      }}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{'Quote detail'}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Date:{' '}
          {moment.unix(quote.t).format('D/M/YYYY H:mm') +
            ' ( ' +
            moment.unix(quote.t).fromNow() +
            ' )'}
        </DialogContentText>
        <DialogContentText id="alert-dialog-description">
          Location:{' '}
          <button className="dialog-button-link" onClick={showLocation}>
            {quote.address}
          </button>
        </DialogContentText>
        <div className="nonce">
          <DialogContentText id="alert-dialog-description">
            Nonce:{' '}
            <a
              href={`https://alphanet.tzscan.io/${quote.opHashOrProvider ||
                info.provider}`}
              target="_blank"
              rel="noopener noreferrer"
              className="dialog-nonce"
            >
              {quote.nonce}
            </a>
          </DialogContentText>
        </div>
        <DialogContentText id="alert-dialog-description">
          Payout: {mtezToTez(quote.payout) + 'ꜩ'}
        </DialogContentText>
        <DialogContentText id="alert-dialog-description">
          Price: {mtezToTez(quote.price) + 'ꜩ'}
        </DialogContentText>
        <DialogContentText
          id="alert-dialog-description"
          style={{ color: mapStatusToColour(quote) }}
        >
          Status: {quote.status}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={toggleModal}
          color="primary"
          variant="contained"
          autoFocus
        >
          OK
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export const NotPaid = withStyles(styles)(
  connect(
    null,
    mapActionsToProps
  )(NotPaidRow)
)
export const ResolvedResolving = withStyles(styles)(
  connect(
    null,
    mapActionsToProps
  )(ResolvedResolvingRow)
)
