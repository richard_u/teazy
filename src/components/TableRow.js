import React, { Component } from 'react'
import moment from 'moment'
import { mapStatusToColour } from '../utils'
import { NotPaid, ResolvedResolving } from './Rows'

import '../assets/css/Table.css'

class TableRow extends Component {
  state = { open: false }

  toggleModal = () => {
    this.setState({ open: !this.state.open })
  }

  renderRow = () => {
    const { quote, info } = this.props
    if (quote.status === 'NOT PAID') {
      return (
        <NotPaid
          toggleModal={this.toggleModal}
          open={this.state.open}
          quote={quote}
          toggleWindow={this.props.toggleWindow}
        />
      )
    } else {
      return (
        <ResolvedResolving
          toggleModal={this.toggleModal}
          open={this.state.open}
          quote={quote}
          info={info}
          toggleWindow={this.props.toggleWindow}
        />
      )
    }
  }

  render() {
    const { quote } = this.props
    return (
      <div>
        <div
          className="table-row"
          style={{ backgroundColor: mapStatusToColour(quote) }}
          onClick={this.toggleModal}
        >
          <div>
            {moment.unix(quote.t).format('D/M/YYYY H:mm') +
              ' ( ' +
              moment.unix(quote.t).fromNow() +
              ' )'}
          </div>
          <div>{quote.address}</div>
        </div>
        {this.renderRow()}
      </div>
    )
  }
}

export default TableRow
