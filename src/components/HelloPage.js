import React from 'react'
import { Button, withStyles } from '@material-ui/core'

import '../assets/css/HelloPage.css'

const styles = theme => ({
  button: {
    textAlign: 'center',
    margin: theme.spacing.unit
  },
  input: {
    display: 'none'
  }
})

const HelloPage = ({ classes, handleOpen }) => (
  <div className="hello-page">
    <div className="hello-page-center">
      <h1 className="hello-page-title">SCL Insurance</h1>
      <h3 className="hello-page-by">
        by{' '}
        <a
          className="hello-page-link"
          href="https://smartcontractlabs.ee/"
          target="_blank"
          rel="noopener noreferrer"
        >
          Smart Contract Labs
        </a>
      </h3>
      <div className="hello-page-content">
        <p>Insurance on the blockchain.</p>
        <p>
          We bring you the insurance for the 21st century. There's no paperwork
          and thanks to the blockchain, our products are reliable and
          predictable. Claims are automatic and resolved in minutes.
        </p>
        <p>
          Our product lets you insure against rainy weather. If bad weather
          ruins your vacation, you'll get compensated.
        </p>
      </div>
      <div className="hello-page-button">
        <Button
          onClick={handleOpen}
          variant="contained"
          color="secondary"
          className={classes.button}
        >
          TRY
        </Button>
      </div>
    </div>
  </div>
)

export default withStyles(styles)(HelloPage)
