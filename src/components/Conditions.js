import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { addAddress } from '../actions/map-actions'
import { addQuote, makeNewQuotesStore } from '../actions/prepareapp-actions'
import { get } from '../utils'
import { apiServer } from '../config'
import { encode } from '../payreq'
import Paper from '@material-ui/core/Paper'
import ErrorSnack from './ErrorSnack'
import SimpleSpinner from './SimpleSpinner'
import ConditionsTable from './ConditionsTable'
import ClearIcon from '@material-ui/icons/Clear'

import '../assets/css/Conditions.css'
import { Button } from '@material-ui/core'

class Conditions extends Component {
  state = {
    error: null,
    open: false,
    request: null,
    quote: null,
    loading: false
  }

  componentDidMount = () => {
    this.getQuote()
  }

  static getDerivedStateFromProps = (props, state) => {
    if (props.latLng !== state.latLng || props.dateTime !== state.dateTime) {
      return {
        latLng: props.latLng,
        dateTime: props.dateTime,
        quote: null,
        request: null
      }
    }
    return null
  }

  componentDidUpdate(prevProps, _prevState) {
    if (
      this.props.latLng !== prevProps.latLng ||
      this.props.dateTime !== prevProps.dateTime
    ) {
      this.getQuote()
    }
  }

  storeQuote = encoded => {
    const { quote } = this.state
    const { address, addQuote, latLng, payout, info } = this.props
    const object = {
      encoded,
      address,
      t: quote.t,
      latLng,
      nonce: quote.nonce,
      opHashOrProvider: info.provider,
      expiration: quote.expiration,
      payout: payout,
      price: quote.price,
      status: 'NOT PAID'
    }
    addQuote(object)
  }

  encodeQuote = quote => {
    if (quote.payreq) {
      try {
        const encoded = encode('alphanet', JSON.stringify(quote.payreq))
        this.storeQuote(encoded)
        this.setState({ request: encoded })
      } catch (error) {
        this.setState({ error: error[1], open: true })
      }
    } else {
      this.setState({ error: 'Quote not found', open: true })
    }
  }

  getQuote = async () => {
    const { dateTime, latLng, payout } = this.props
    this.setState({ loading: true })
    try {
      const response = await get(
        `${apiServer}/quote?lat=${latLng.lat}&lon=${
          latLng.lng
        }&time=${dateTime.unix()}&payout=${payout}`
      )
      this.setState({
        quote: response,
        loading: false
      })
      this.encodeQuote(response)
    } catch (error) {
      this.setState({ error, open: true, loading: false })
    }
  }

  toggleSnack = () => {
    this.setState({ open: !this.state.open })
  }

  linkToWalletPage = () => {
    window.open(`https://tw.smartcontractlabs.ee/r/#${this.state.request}`)
  }

  renderContent = () => {
    return (
      <>
        <div>
          {this.state.request ? (
            <div className="sidebar-button-wrapper">
              <Button
                className="sidebar-button"
                variant="contained"
                color="secondary"
                onClick={this.linkToWalletPage}
              >
                Pay with SCL Web Wallet
              </Button>
            </div>
          ) : null}
        </div>

        {this.state.loading ? (
          <div className="overview-spinner">
            <SimpleSpinner size={18} thickness={4} />
          </div>
        ) : null}
      </>
    )
  }

  render() {
    const { address, dateTime, payout } = this.props
    return (
      <Paper square className="paper-conditions">
        <div className="icon-wrapper">
          <ClearIcon className="icon-clear" onClick={this.props.toggleWindow} />
        </div>
        <div className="bubble-name">Your Terms</div>
        <ConditionsTable
          address={address}
          dateTime={dateTime}
          quote={this.state.quote}
          payout={payout}
        />
        {this.renderContent()}
        <ErrorSnack
          open={this.state.open}
          handleClose={this.toggleSnack}
          error={this.state.error}
        />
      </Paper>
    )
  }
}

Conditions.propTypes = {
  addAddress: PropTypes.func,
  address: PropTypes.string,
  addQuote: PropTypes.func,
  dateTime: PropTypes.object,
  latLng: PropTypes.object,
  makeNewQuotesStore: PropTypes.func,
  payout: PropTypes.number,
  toggleWindow: PropTypes.func
}

const mapStateToProps = state => {
  return {
    address: state.map.address,
    dateTime: state.date.dateTime,
    latLng: state.map.latLng,
    info: state.app.info
  }
}

const mapActionsToProps = {
  addAddress,
  addQuote,
  makeNewQuotesStore
}

export default connect(
  mapStateToProps,
  mapActionsToProps
)(Conditions)
