import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import InputBase from '@material-ui/core/InputBase'
import IconButton from '@material-ui/core/IconButton'
import ListIcon from '@material-ui/icons/List'
import SearchIcon from '@material-ui/icons/Search'

const styles = {
  root: {
    display: 'flex',
    alignItems: 'center',
    width: 250,
    margin: 5,
    position: 'absolute'
  },
  input: {
    marginLeft: 8,
    flex: 1,
    fontSize: 13
  },
  iconButton: {
    padding: 5
  }
}

const CustomizedInputBase = ({
  classes,
  openQuotes,
  onBlur,
  onChange,
  onClick,
  onKeyDown,
  placeholder,
  value
}) => (
  <Paper className={classes.root} elevation={1}>
    <IconButton
      className={classes.iconButton}
      aria-label="Menu"
      onClick={openQuotes}
    >
      <ListIcon />
    </IconButton>
    <InputBase
      className={classes.input}
      placeholder={placeholder}
      onChange={onChange}
      value={value}
      onKeyDown={onKeyDown}
      onBlur={onBlur}
    />
    <IconButton
      className={classes.iconButton}
      aria-label="Search"
      onClick={() => onClick(value)}
    >
      <SearchIcon />
    </IconButton>
  </Paper>
)

CustomizedInputBase.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(CustomizedInputBase)
