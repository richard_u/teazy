import React from 'react'
import PropTypes from 'prop-types'
import DateInput from 'react-datepicker'

import 'react-datepicker/dist/react-datepicker.css'
import '../assets/css/Datetime.css'

const TimePicker = ({ date, onChangeDate }) => (
  <DateInput
    inline
    selected={date}
    onChange={date => onChangeDate(date)}
    showTimeSelect
    showTimeSelectOnly
    timeIntervals={15}
    timeFormat="HH:mm"
  />
)

TimePicker.propTypes = {
  date: PropTypes.object,
  onChangeDate: PropTypes.func
}

export default TimePicker
