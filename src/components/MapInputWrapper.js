import React from 'react'
import { connect } from 'react-redux'
import { addAddress, addLatLng } from '../actions/map-actions'
import {
  makeNewQuotesStore,
  makeNewQuotesObject,
  storeInfoResponse
} from '../actions/prepareapp-actions'
import moment from 'moment'
import { geocodeByAddress, getLatLng } from 'react-places-autocomplete'
import { get, mic2arr, getHistory, nonceToOp } from '../utils'
import { apiServer } from '../config'
import Geocode from 'react-geocode'
import { Paper, Button, Fade, withStyles } from '@material-ui/core'
import ErrorSnack from './ErrorSnack'
import DateTimeWrapper from './DateTimeWrapper'
import FullScreenDialog from './FullScreenDialog'
import LeafletMap from './LeafletMap'
import LocationInput from './LocationInput'
import Conditions from './Conditions'
import ClearIcon from '@material-ui/icons/Clear'
import IconButton from '@material-ui/core/IconButton'
import ListIcon from '@material-ui/icons/List'

import '../assets/css/Map.css'
import '../assets/css/Autocomplete.css'

// Geocode.setApiKey(process.env.REACT_APP_GOOGLE_DEV_APIKEY)
Geocode.setApiKey('AIzaSyChTSkRuidTwL_2TxA75wZ1wNCPN_7u_-s')

const styles = {
  root: {
    display: 'flex',
    alignItems: 'center',
    margin: 5,
    position: 'absolute'
  },
  iconButton: {
    padding: 5
  }
}
class MapInputWrapper extends React.Component {
  state = {
    error: null,
    open: false,
    payout: 250000000,
    showOverview: false,
    showQuotes: false,
    showMenu: false,
    expanded: null,
    interval: null,
    storage: [],
    viewport: {
      center: Object.values(this.props.latLng),
      zoom: 13
    }
  }

  componentDidMount = () => {
    this.getInfoObject()
    this.getStorage()
    const interval = setInterval(() => this.getStorage(), 30000)
    this.setState({
      interval,
      error: 'Search or click the map to choose a location.',
      open: true
    })
  }

  componentWillUnmount = () => {
    clearInterval(this.state.interval)
  }

  static getDerivedStateFromProps = (props, state) => {
    if (props.storage.length !== state.storage.length) {
      return {
        storage: props.storage
      }
    }
    return null
  }

  componentDidUpdate(prevProps, _prevState) {
    if (this.props.storage.length !== prevProps.storage.length) {
      this.getStorage()
    }
  }

  getInfoObject = async () => {
    try {
      const response = await get(
        'https://insurance-api.smartcontractlabs.ee/info'
      )
      this.props.storeInfoResponse(response)
    } catch (error) {
      this.setState({ error, open: true })
    }
  }

  checkForResolved = quote => {
    return (
      moment.unix(quote.t).isBefore(moment()) && quote.status !== 'RESOLVED'
    )
  }

  checkForStorageNonce = contract => {
    const {
      makeNewQuotesObject,
      makeNewQuotesStore,
      storage,
      info
    } = this.props
    const queued = [],
      resolving = [],
      resolved = []
    const parsedStorage = mic2arr(contract)
    if (parsedStorage) {
      const store = storage.reduce((acc, quote) => {
        if (quote.status === 'NOT PAID' || this.checkForResolved(quote)) {
          const stringifiedStorage = JSON.stringify(parsedStorage)
          const nonceStatus = stringifiedStorage.includes(quote.nonce)
          if (nonceStatus) {
            getHistory(
              moment(quote.expiration * 1000)
                .subtract(25, 'minutes')
                .toISOString(),
              info.provider
            )
              .then(
                history =>
                  (quote.opHashOrProvider = nonceToOp(quote.nonce, history))
              )
              .catch(error => {
                console.log(error)
              })
            quote.status = 'PAID'
            resolving.push(quote)
            acc.push(quote)
          } else if (!nonceStatus && quote.status === 'PAID') {
            quote.status = 'RESOLVED'
            resolved.push(quote)
            acc.push(quote)
          } else if (quote.status === 'RESOLVED') {
            resolved.push(quote)
            acc.push(quote)
          } else if (moment.unix(quote.expiration).isAfter(moment())) {
            queued.push(quote)
            acc.push(quote)
          }
        } else {
          if (quote.status === 'PAID') {
            resolving.push(quote)
            acc.push(quote)
          } else {
            resolved.push(quote)
            acc.push(quote)
          }
        }
        return acc
      }, [])
      this.createLocalStorageObject(store)
      makeNewQuotesObject(queued, resolving, resolved)
      makeNewQuotesStore(store)
    }
  }

  createLocalStorageObject = quotes => {
    setTimeout(() => {
      localStorage.removeItem('quotes')
      localStorage.setItem('quotes', JSON.stringify(quotes))
    }, 1000)
  }

  getStorage = async () => {
    try {
      const responseInfo = await get(`${apiServer}/info`)
      const response = await get(
        `https://alphanet.smartcontractlabs.ee/chains/main/blocks/head/context/contracts/${
          responseInfo.provider
        }/storage`
      )
      this.checkForStorageNonce(response)
    } catch (error) {
      console.log('ERROR', error)
    }
  }

  handleChangeInput = address => {
    this.props.addAddress(address)
  }

  handleSelect = address => {
    geocodeByAddress(address)
      .then(result => {
        this.setState({ address: result[0].formatted_address })
        this.props.addAddress(result[0].formatted_address)
        return getLatLng(result[0])
      })
      .then(latLng => {
        this.props.addLatLng(latLng)
        this.setState({ showMenu: true, showOverview: false })
      })
      .catch(error => this.setState({ error }))
  }

  getAddress = latLng => {
    Geocode.fromLatLng(latLng.lat, latLng.lng).then(
      response => {
        const address = response.results[0].formatted_address
        this.props.addAddress(address)
      },
      error => {
        this.setState({ error, open: true })
      }
    )
  }

  handleMapClick = e => {
    const latLng = e.latlng
    this.props.addLatLng(latLng)
    this.getAddress(latLng)
    this.setState({ showMenu: true, showOverview: false, open: false })
  }

  toggleSnack = () => {
    this.setState({ open: !this.state.open })
  }

  showOverview = () => {
    const { address } = this.props
    if (address.length !== 0) {
      this.setState({
        showMenu: false,
        showOverview: !this.state.showOverview,
        expanded: null
      })
    } else {
      this.setState({
        error: 'Make sure to enter address, date and time.',
        open: true
      })
    }
  }

  showQuotes = () => {
    this.setState({
      showQuotes: !this.state.showQuotes,
      expanded: null,
      open: false
    })
  }

  showMenu = () => {
    this.setState({
      showMenu: !this.state.showMenu,
      expanded: null
    })
    if (this.state.showOverview) {
      this.setState({ showOverview: false })
    }
  }

  handleChange = panel => (_e, expanded) => {
    if (expanded) {
      this.setState({ expanded: panel, showOverview: false })
    } else {
      this.setState({ expanded: false })
    }
  }

  controlViewport = viewport => {
    this.setState({ viewport })
  }

  changePayout = (_e, payout) => {
    this.setState({ payout, showOverview: false })
  }

  render() {
    const { address, latLng, classes } = this.props
    return (
      <div className="main-screen">
        {this.state.showMenu || this.state.showOverview ? (
          <div className="location-input">
            <Paper className={classes.root} elevation={1}>
              <IconButton
                className={classes.iconButton}
                aria-label="Menu"
                onClick={this.showQuotes}
              >
                <ListIcon />
              </IconButton>
            </Paper>
          </div>
        ) : (
          <div className="location-input">
            <LocationInput
              address={address}
              handleChange={this.handleChangeInput}
              handleSelect={this.handleSelect}
              openQuotes={this.showQuotes}
            />
          </div>
        )}
        <div className="sidebar">
          {this.state.showMenu ? (
            <Paper className="sidebar-paper">
              <div className="icon-wrapper">
                <ClearIcon className="icon-clear" onClick={this.showMenu} />
              </div>
              <div className="bubble-name">Customize coverage</div>
              <DateTimeWrapper
                handleChange={this.handleChange}
                expanded={this.state.expanded}
                changePayout={this.changePayout}
                payout={this.state.payout}
              />
              <div className="sidebar-button-wrapper">
                <Button
                  className="sidebar-button"
                  variant="contained"
                  color="primary"
                  onClick={this.showOverview}
                >
                  Get terms
                </Button>
              </div>
            </Paper>
          ) : null}
          {this.state.showOverview ? (
            <Fade in={this.state.showOverview}>
              <Conditions
                toggleWindow={this.showOverview}
                payout={this.state.payout}
              />
            </Fade>
          ) : null}
          {this.state.showQuotes ? (
            <FullScreenDialog
              open={this.state.showQuotes}
              toggleWindow={this.showQuotes}
            />
          ) : null}
        </div>
        <LeafletMap
          latLng={latLng}
          handleClick={this.handleMapClick}
          viewport={this.state.viewport}
          controlViewport={this.controlViewport}
        />
        <ErrorSnack
          open={this.state.open}
          handleClose={this.toggleSnack}
          error={this.state.error}
        />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    address: state.map.address,
    latLng: state.map.latLng,
    storage: state.app.storage,
    info: state.app.info
  }
}

const mapActionsToProps = {
  addAddress,
  addLatLng,
  makeNewQuotesStore,
  makeNewQuotesObject,
  storeInfoResponse
}

export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapActionsToProps
  )(MapInputWrapper)
)
