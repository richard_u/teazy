import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import moment from 'moment'
import { newDateTime } from '../actions/date-actions'
import { withStyles } from '@material-ui/core/styles'
import { mtezToTez } from '../utils'
import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import Typography from '@material-ui/core/Typography'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import DatePicker from './DatePicker'
import TimePicker from './TimePicker'
import Slider from '@material-ui/lab/Slider'
import Paper from '@material-ui/core/Paper'

import '../assets/css/Datetime.css'

const styles = theme => ({
  root: {
    width: '100%'
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary
  }
})

class DateTimeWrapper extends Component {
  componentDidMount = () => {
    this.checkDateTime()
  }

  checkDateTime = () => {
    const { dateTime } = this.props
    if (moment(dateTime) < moment().add(3, 'minutes')) {
      this.props.newDateTime(moment().add(20, 'minutes'))
    }
  }

  onChangeDate = dateTime => {
    this.props.newDateTime(dateTime)
  }

  render() {
    const {
      classes,
      payout,
      dateTime,
      expanded,
      changePayout,
      handleChange
    } = this.props
    return (
      <div className="centerPaperWrapper">
        <Paper square className="paper-payout" elevation={4}>
          <p className="slider-label">Payout</p>
          <Slider
            className="wrapperSlider"
            min={5000000}
            max={500000000}
            step={10000000}
            value={payout}
            aria-labelledby="label"
            onChange={changePayout}
          />
          <div className="slider-value">{`${mtezToTez(payout)} ꜩ`}</div>
        </Paper>
        <ExpansionPanel
          expanded={expanded === 'panel1'}
          onChange={handleChange('panel1')}
        >
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>Date</Typography>
            <Typography className={classes.secondaryHeading}>
              {moment(dateTime).format('D/M/YYYY')}
            </Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <div className="date-wrapper">
              <DatePicker date={dateTime} onChangeDate={this.onChangeDate} />
            </div>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel
          expanded={expanded === 'panel2'}
          onChange={handleChange('panel2')}
        >
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <Typography className={classes.heading}>Time</Typography>
            <Typography className={classes.secondaryHeading}>
              {moment(dateTime).format('HH:mm')}
            </Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <div className="time-wrapper">
              <TimePicker date={dateTime} onChangeDate={this.onChangeDate} />
            </div>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </div>
    )
  }
}

DateTimeWrapper.propType = {
  classes: PropTypes.object.isRequired,
  dateTime: PropTypes.string,
  newDateTime: PropTypes.func,
  newPayout: PropTypes.func,
  payout: PropTypes.number,
  expanded: PropTypes.bool,
  handleChange: PropTypes.func
}

const mapStateToProps = state => {
  return {
    dateTime: state.date.dateTime
  }
}

const mapActionsToProps = {
  newDateTime
}

export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapActionsToProps
  )(DateTimeWrapper)
)
