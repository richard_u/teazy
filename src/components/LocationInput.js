import React from 'react'
import PlacesAutocomplete from 'react-places-autocomplete'
import { withStyles } from '@material-ui/core'
import indigo from '@material-ui/core/colors/indigo'

import '../assets/css/Autocomplete.css'
import GoogleInput from './GoogleInput'

const styles = {
  root: {
    width: '240px',
    padding: '8px',
    fontFamily: 'system-ui',
    fontSize: '13px',
    backgroundColor: 'white',
    boxShadow:
      '0px 1px 5px 0px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12)',
    '&&&&:hover:before': {
      borderBottom: '2px solid #3f51b5'
    }
  },
  input: {
    width: '90%'
  },
  iconHover: {
    cursor: 'pointer',
    position: 'absolute',
    color: 'rgba(0, 0, 0, 0.54)',
    right: '40px',
    top: '10px',
    '&:hover': {
      color: indigo[800]
    }
  }
}

const LocationInput = ({ address, handleChange, handleSelect, openQuotes }) => (
  <PlacesAutocomplete
    value={address}
    onChange={handleChange}
    onSelect={handleSelect}
    shouldFetchSuggestions={address.length > 3}
  >
    {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
      <div className="autocomplete">
        <GoogleInput
          {...getInputProps({
            placeholder: 'Search Places...',
            onClick: handleSelect,
            openQuotes: openQuotes
          })}
        />
        <div className="autocomplete-dropdown">
          {loading && <div>Loading...</div>}
          {suggestions.map(suggestion => {
            const className = suggestion.active
              ? 'autocomplete-suggestion-active'
              : 'autocomplete-suggestion'
            return (
              <div
                {...getSuggestionItemProps(suggestion, {
                  className
                })}
              >
                <span>{suggestion.description}</span>
              </div>
            )
          })}
        </div>
      </div>
    )}
  </PlacesAutocomplete>
)

export default withStyles(styles)(LocationInput)
