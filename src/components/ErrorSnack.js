import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Snackbar from '@material-ui/core/Snackbar'
import ErrorIcon from '@material-ui/icons/Error'
import CloseIcon from '@material-ui/icons/Close'
import IconButton from '@material-ui/core/IconButton'

const styles = theme => ({
  root: {
    padding: '6px 8px'
  },
  action: {
    paddingLeft: 0
  },
  close: {
    padding: theme.spacing.unit / 2,
    paddingLeft: 10
  },
  error: {
    backgroundColor: theme.palette.error.dark
  },
  message: {
    display: 'flex',
    alignItems: 'center'
  },
  icon: {
    fontSize: 15,
    opacity: 0.9,
    marginRight: theme.spacing.unit
  }
})

const SimpleSnackbar = ({ classes, error, handleClose, open }) => (
  <Snackbar
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center'
    }}
    open={open}
    autoHideDuration={6000}
    onClose={handleClose}
    ContentProps={{
      classes: {
        message: classes.message,
        action: classes.action,
        root: classes.root
      }
    }}
    message={
      <span id="client-snackbar">
        <ErrorIcon className={classes.icon} />
        {`${error}`}
      </span>
    }
    action={[
      <IconButton
        key="close"
        aria-label="Close"
        color="inherit"
        className={classes.close}
        onClick={handleClose}
      >
        <CloseIcon />
      </IconButton>
    ]}
  />
)

SimpleSnackbar.propTypes = {
  classes: PropTypes.object.isRequired,
  error: PropTypes.string,
  handleClose: PropTypes.func,
  open: PropTypes.bool
}

export default withStyles(styles)(SimpleSnackbar)
