import React from 'react'
import PropTypes from 'prop-types'
import DateInput from 'react-datepicker'
import moment from 'moment'

import 'react-datepicker/dist/react-datepicker.css'

const DatePicker = ({ date, onChangeDate }) => (
  <DateInput
    inline
    selected={date}
    onChange={date => onChangeDate(date)}
    dateFormat="LLL"
    minDate={moment()}
    maxDate={moment().add(6, 'days')}
  />
)

DatePicker.propTypes = {
  date: PropTypes.object,
  onChangeDate: PropTypes.func
}

export default DatePicker
