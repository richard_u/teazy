import React, { Component } from 'react'
import MapInputWrapper from './MapInputWrapper'
import HelloPage from './HelloPage'
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core'

const theme = createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    primary: {
      light: '#5e7198',
      main: '#31466a',
      dark: '#001f3f',
      contrastText: '#fff'
    },
    secondary: {
      light: '#ffb650',
      main: '#ff851b',
      dark: '#c55600',
      contrastText: '#000'
    }
  }
})

class Root extends Component {
  state = { open: false }

  handleOpen = () => {
    this.setState({ open: true })
  }

  render() {
    return this.state.open ? (
      <MuiThemeProvider theme={theme}>
        <MapInputWrapper />
      </MuiThemeProvider>
    ) : (
      <MuiThemeProvider theme={theme}>
        <HelloPage handleOpen={this.handleOpen} />
      </MuiThemeProvider>
    )
  }
}

export default Root
