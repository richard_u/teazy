export const get = async url => {
  const response = await fetch(url)
  if (!response.ok) throw await response.text()
  return await response.json()
}

export const post = async (url, body) => {
  const response = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body
  })
  if (!response.ok) throw await response.text()
  return await response.json()
}

export const mic2arr = s => {
  let ret = []
  if (Object.prototype.hasOwnProperty.call(s, 'prim')) {
    if (s.prim === 'Pair') {
      ret.push(mic2arr(s.args[0]))
      ret = ret.concat(mic2arr(s.args[1]))
    } else if (s.prim === 'Elt') {
      ret = {
        key: mic2arr(s.args[0]),
        val: mic2arr(s.args[1])
      }
    } else if (s.prim === 'True') {
      ret = true
    } else if (s.prim === 'False') {
      ret = false
    }
  } else if (Array.isArray(s)) {
    const sc = s.length
    for (let i = 0; i < sc; i++) {
      const n = mic2arr(s[i])
      if (typeof n.key !== 'undefined') {
        if (Array.isArray(ret)) {
          ret = {
            keys: [],
            vals: []
          }
        }
        ret.keys.push(n.key)
        ret.vals.push(n.val)
      } else {
        ret.push(n)
      }
    }
  } else if (Object.prototype.hasOwnProperty.call(s, 'string')) {
    ret = s.string
  } else if (Object.prototype.hasOwnProperty.call(s, 'int')) {
    ret = parseInt(s.int, 10)
  } else {
    ret = s
  }
  return ret
}

export const mapStatusToColour = quote => {
  switch (quote.status) {
    case 'NOT PAID':
      return '#ff851b'
    case 'PAID':
      return '#47a9cc'
    case 'RESOLVED':
      return '#01FF70'
    default:
      return '#FF4136'
  }
}

export const mtezToTez = mtez => {
  return mtez / 1000000
}

export const getHistory = (timestamp, provider, p) => {
  if (typeof p === 'undefined') {
    p = 0
  }
  return fetch(
    `https://api.alphanet.tzscan.io/v1/operations/${provider}?type=Transaction&p=${p}`
  )
    .then(r => r.text())
    .then(r => JSON.parse(r))
    .then(r => {
      let result = []
      if (r === []) {
        return result
      }
      let t = new Date(Date.now()).toISOString()
      for (const batch of r) {
        for (const op of batch.type.operations) {
          if (op.failed !== false) {
            continue
          }
          if (op.timestamp > timestamp) {
            op.hash = batch.hash
            result.push(op)
          }
          if (t > op.timestamp) {
            t = op.timestamp
          }
        }
      }
      if (t < timestamp) {
        return result
      }
      return getHistory(timestamp, ++p).then(arr => result.concat(arr))
    })
}

export const nonceToOp = (n, history) => {
  for (const op of history) {
    if (typeof op.str_parameters === 'undefined') {
      continue
    }
    if (op.str_parameters.includes(n)) {
      return op.hash
    }
  }
}
