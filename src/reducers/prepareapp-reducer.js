import {
  ADD_QUOTE,
  DELETE_INFO_SERVER_RESPONSE,
  GET_CONTRACT_STORAGE,
  MAKE_NEW_QUOTES_STORE,
  MAKE_NEW_QUOTES_OBJECT,
  REHYDRATE_QUOTES_FROM_STORAGE,
  STORE_INFO_SERVER_RESPONSE
} from '../actions/prepareapp-actions'

const initialState = {
  info: null,
  storage: JSON.parse(localStorage.getItem('quotes')) || [],
  contract: null,
  quotes: {
    queued: [],
    resolving: [],
    resolved: []
  }
}

const prepareAppReducer = (state = initialState, { payload, type }) => {
  switch (type) {
    case ADD_QUOTE:
      localStorage.setItem(
        'quotes',
        JSON.stringify([...state.storage, payload.quote])
      )
      return { ...state, storage: [...state.storage, payload.quote] }
    case DELETE_INFO_SERVER_RESPONSE:
      return { ...state, info: null }
    case GET_CONTRACT_STORAGE:
      return { ...state, contract: payload.storage }
    case MAKE_NEW_QUOTES_OBJECT:
      return {
        ...state,
        quotes: {
          queued: payload.queued,
          resolving: payload.resolving,
          resolved: payload.resolved
        }
      }
    case MAKE_NEW_QUOTES_STORE:
      return { ...state, storage: payload.quotes }
    case STORE_INFO_SERVER_RESPONSE:
      return { ...state, info: payload }
    case REHYDRATE_QUOTES_FROM_STORAGE:
      return { ...state, storage: payload.quotes }
    default:
      return state
  }
}

export default prepareAppReducer
