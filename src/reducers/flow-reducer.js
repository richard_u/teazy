import {
  NEXT_STEP,
  PREVIOUS_STEP,
  RESET_STEPPER,
  TOGGLE_DIALOG
} from '../actions/flow-actions'

const initialState = {
  labels: ['Prepare', 'Location', 'Conditions', 'Receipt'],
  step: 0,
  open: false
}

const flowReducer = (state = initialState, { type }) => {
  switch (type) {
    case NEXT_STEP:
      return { ...state, step: state.step + 1 }
    case PREVIOUS_STEP:
      return { ...state, step: state.step - 1 }
    case RESET_STEPPER:
      return { ...state, step: 0 }
    case TOGGLE_DIALOG:
      return { ...state, open: !state.open }
    default:
      return state
  }
}

export default flowReducer
