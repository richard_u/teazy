import {
  ADD_ADDRESS,
  ADD_LATLNG,
  REMOVE_ADDRESS_AND_LATLNG
} from '../actions/map-actions'

const initialState = {
  address: '',
  latLng: {
    lat: 51.500975,
    lng: -0.126702
  }
}

const flowReducer = (state = initialState, { payload, type }) => {
  switch (type) {
    case ADD_ADDRESS:
      return { ...state, address: payload.address }
    case ADD_LATLNG:
      return { ...state, latLng: payload.latLng }
    case REMOVE_ADDRESS_AND_LATLNG:
      return initialState
    default:
      return state
  }
}

export default flowReducer
