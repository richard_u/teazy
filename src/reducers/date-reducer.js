import moment from 'moment'
import {
  NEW_DATETIME,
  NEW_PAYOUT,
  RESET_DATETIMEPAYOUT
} from '../actions/date-actions'

const initialState = {
  dateTime: moment().add(20, 'minutes'),
  payout: 250
}

const dateReducer = (state = initialState, { payload, type }) => {
  switch (type) {
    case NEW_DATETIME:
      return { ...state, dateTime: payload.dateTime }
    case NEW_PAYOUT:
      return { ...state, payout: payload.payout }
    case RESET_DATETIMEPAYOUT:
      return { ...state, dateTime: moment(), payout: 250 }
    default:
      return state
  }
}

export default dateReducer
