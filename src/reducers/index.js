import { combineReducers } from 'redux'
import dateReducer from './date-reducer'
import flowReducer from './flow-reducer'
import mapReducer from './map-reducer'
import prepareAppReducer from './prepareapp-reducer'

const reducers = combineReducers({
  app: prepareAppReducer,
  date: dateReducer,
  flow: flowReducer,
  map: mapReducer
})

export default reducers
