## Insurance on the blockchain. (Web UI)

### Proof of Concept

We bring you the insurance for the 21st century. There's no paperwork thanks to the blockchain.

Our product lets you insure against rainy weather. If bad weather ruins your vacation, you'll get compensated.
